package com.example.mcservertracker.ui.main.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class PingResponse {

    @SerializedName("description")
    @Expose
    private val description: Description? = null

    @SerializedName("players")
    @Expose
    private val players: Players? = null

    @SerializedName("version")
    @Expose
    private val version: Version? = null


    class Description {
        @SerializedName("text")
        @Expose
        var text: String? = null
    }

    class Version {
        @SerializedName("text")
        @Expose
        var name: String? = null

        @SerializedName("protocol")
        @Expose
        var protocol = 0
    }
}