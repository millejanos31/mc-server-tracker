package com.example.mcservertracker.ui.main

import android.content.Context
import android.net.Uri
import android.os.Handler
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mcservertracker.R
import com.example.mcservertracker.ui.main.models.ServerUI
import com.example.mcservertracker.ui.main.view.ServerListFragment
import io.reactivex.rxjava3.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_server_card_detail.view.*
import kotlinx.android.synthetic.main.item_server_card_main.view.*


class ServerRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TAG = ServerListFragment::class.simpleName
    private var items: ArrayList<ServerUI> = ArrayList()
    private var openedCardIp: String? = null
    private var closeOpenedCardSubject = PublishSubject.create<String>()
    private var animationInProgress = false;

    companion object {
        const val DETAIL_CARD_ANIMATION_DURATION = 300L
        const val DETAIL_CARD_MIN_HEIGHT: Int = 170
        const val DETAIL_CARD_MAX_HEIGHT: Int = 650
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ServerViewHolder(
            parent.context,
            LayoutInflater.from(parent.context).inflate(R.layout.item_server_card, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {
            is ServerViewHolder -> {
                holder.bind(items[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun submitList(serverList: ArrayList<ServerUI>) {
        items = serverList
    }

    fun add(serverList: ArrayList<ServerUI>) {
        Log.d(TAG, "items: add (+"+serverList.size+")");
        items.addAll(serverList)
    }

    fun add(server : ServerUI) {
        Log.d(TAG, "items: add (+1)");
        items.add(server)
    }

    fun update(index : Int, server : ServerUI) {
        Log.d(TAG, "items: update [index: $index]");
        if (index >= 0 && index < items.size) {
            items[index] = server
            notifyItemChanged(index);
        } else {
            Log.e(TAG, "ERROR: updating item at index of: $index, size of the list: " + items.size)
        }
    }

    fun contains(ip : String?, dnsIp : String?): Boolean {
        if (ip == null && dnsIp == null) {
            Log.w(TAG, "ERROR: Contains: Server IP & DNS IP is null.")
            return false;
        }
        for (item in items) {
            if ((item.ip != null && item.ip == ip)
                || (item.dnsIp != null && item.dnsIp == dnsIp)) {
                return true;
            }
        }
        return false;
    }

    fun getSize() = items.size

    fun getAllItems(): ArrayList<ServerUI> {
        return items
    }

    fun getServerIndexByIp(ip: String): Int {
        for ((idx, item: ServerUI) in items.withIndex()) {
            if (item.hasSameIpAs(ip)) {
                return idx
            }
        }
        return -1
    }

    inner class ServerViewHolder (val context: Context, itemView: View): RecyclerView.ViewHolder(itemView) {

        private val serverNameTV: TextView = itemView.server_card_name_text_view
        private val serverMotdTV: TextView = itemView.server_card_motd_text_view
        private val serverPingTV: TextView = itemView.server_card_ping_text_view
        private val serverPlayersTV: TextView = itemView.server_card_players_text_view
        private val serverPingImageIV: ImageView = itemView.server_card_ping_image_view
        private val serverThumbnailImageIV: ImageView = itemView.server_card_thumbnail_image_view
        private val serverMainCardContainerCV: CardView = itemView.server_main_card_container
        private val serverDetailCardContainerCV: CardView = itemView.server_detail_card_container

        fun bind(server: ServerUI) {
            serverNameTV.text = when {
                !server.customName.isNullOrEmpty() -> server.customName
                !server.dnsIp.isNullOrEmpty() -> server.dnsIp
                else -> server.ip
            }
            serverPingTV.text = server.getFormattedPing()
            serverPlayersTV.text = server.getFormattedPlayerString()
            serverMotdTV.setText(
                Html.fromHtml(server.getFormattedMotd(), Html.FROM_HTML_MODE_LEGACY),
                TextView.BufferType.SPANNABLE)

            try {
                Glide.with(itemView.context) // TODO context from param OR itemView.context
                    .load(Uri.parse(server.image))
                    .into(serverThumbnailImageIV)
            } catch (e: NullPointerException) {
                //Log.e("ERROR: ServerRecyclerAdapter - load server image for ip: "  + server.dnsIp
                //        + " ("+server.ip+")\nException: ", e.stackTrace.toString())
                Glide.with(itemView.context)
                    .load(R.drawable.default_server_icon)
                    .into(serverThumbnailImageIV)
            }

            when (server.pingState) {
                ServerUI.Companion.PingState.NONE -> {
                    Glide.with(itemView.context)
                        .load(R.drawable.ping_signal_none_hd)
                        .into(serverPingImageIV)
                }
                ServerUI.Companion.PingState.PINGING -> {
                    Glide.with(itemView.context)
                        .load(R.drawable.ping_loading_gif_15_hd)
                        .into(serverPingImageIV)
                }
                ServerUI.Companion.PingState.SUCCESS -> {
                    Glide.with(itemView.context)
                        .load(server.getPingLevelDrawable())
                        .into(serverPingImageIV)
                }
                ServerUI.Companion.PingState.FAILED -> {
                    Glide.with(itemView.context)
                        .load(R.drawable.ping_signal_0_hd)
                        .into(serverPingImageIV)
                }
            }

            restoreLastDetailsCardStateOnUI(server)

            //SPECS (open-close server cards)
            //Click on closed card -> Close every other card, open current one.
            //Click on opened card -> Close current card.
            serverMainCardContainerCV.setOnClickListener {
                if (animationInProgress) {
                    return@setOnClickListener
                }
                if (openedCardIp == null) {
                    // first open
                    openCard(server)
                } else {
                    val lastOpenedIp = openedCardIp
                    closeOpenedCard()
                    if (lastOpenedIp != null && !server.hasSameIpAs(lastOpenedIp)) {
                        openCard(server)
                    }
                }
            }

            listenToCloseCard(server)
        }

        private fun restoreLastDetailsCardStateOnUI(server: ServerUI) {
            if (server.open) {
                serverDetailCardContainerCV.visibility = View.VISIBLE
            } else {
                serverDetailCardContainerCV.visibility = View.GONE
            }
        }

        var showDetailsHandler = Handler()
        var hideDetailsHandler = Handler()

        private fun openDetailsAnimation() {
            animationInProgress = true;
            serverDetailCardContainerCV.visibility = View.VISIBLE

            CustomAnimationUtils.startAnimateViewHeight(serverDetailCardContainerCV,
                DETAIL_CARD_MAX_HEIGHT,
                DETAIL_CARD_ANIMATION_DURATION)

            showDetailsHandler.postDelayed({
                animationInProgress = false;
            }, DETAIL_CARD_ANIMATION_DURATION)

            hideDetailsHandler.removeCallbacksAndMessages(null)
        }

        private fun closeDetailsAnimation() {
            animationInProgress = true;
            CustomAnimationUtils.startAnimateViewHeight(serverDetailCardContainerCV,
                DETAIL_CARD_MIN_HEIGHT,
                DETAIL_CARD_ANIMATION_DURATION)

            hideDetailsHandler.postDelayed({
                    serverDetailCardContainerCV.visibility = View.GONE
                    animationInProgress = false;
                }, DETAIL_CARD_ANIMATION_DURATION)

            showDetailsHandler.removeCallbacksAndMessages(null)
        }

        private fun openCard(server: ServerUI) {
            if (!server.open) {
                openDetailsAnimation()
                server.open = true
                openedCardIp = server.ip
            }
        }

        private fun closeCard(server: ServerUI) {
            if (server.open) {
                server.open = false;
                openedCardIp = null;
                closeDetailsAnimation();
            }
        }

        private fun closeOpenedCard() {
            val openedIp = openedCardIp
            if (openedIp != null) {
                for (item in items) {
                    if (item.open && item.hasSameIpAs(openedIp)) {
                        closeOpenedCardSubject.onNext(item.ip)
                    }
                }
            }
        }

        private fun listenToCloseCard(server: ServerUI) {
            closeOpenedCardSubject.subscribe {
                if (server.hasSameIpAs(it)) {
                    closeCard(server)
                }
            }
        }

    }

}

