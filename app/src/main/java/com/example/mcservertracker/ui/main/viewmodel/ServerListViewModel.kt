package com.example.mcservertracker.ui.main.viewmodel

import androidx.lifecycle.ViewModel
import com.example.mcservertracker.ui.main.ServerDataProvider
import com.example.mcservertracker.ui.main.models.ServerUI
import io.reactivex.Observable

class ServerListViewModel(
    private val provider: ServerDataProvider)
    : ViewModel() {

    fun getServersDataByIpsCold(servers: List<Pair<String, String>>): Observable<ServerUI> {
        return Observable
            .fromIterable(servers)
            .flatMap {
                provider.getServerDataByDnsIp(it.first, it.second)
            }
    }

    fun getPingServerByIpCold(dnsIp: String): Observable<Long> {
        return provider.pingServerByDnsIp(dnsIp)
    }

    /**
     * Getting already added servers from database
     */
    fun getPersistedServerIpList(): List<Pair<String, String>> {
        // TODO read server ip list with SQLite (SQLiteManager.kt)
        return listOf(
            Pair("server15.clans.hu:25595", "My server test name"),
            Pair("hypixel.net", ""),
            Pair("pvp.land", ""),
            Pair("eu.minemen.club", ""),
            Pair("not.existing.ip", ""),
            Pair("play.ecc.eco", ""),
            Pair("watermc.gg", ""),
            Pair("mc.herobrine.org", ""),
            Pair("play.cubecraft.net", ""),
            Pair("pixel.mc-complex.com", ""),
            Pair("81.196.233.214", ""),
            Pair("mc.mcs.gg", ""),
            Pair("play.mcprison.com", ""),
            Pair("mccentral.org", ""),
            Pair("play.pokezone.net", "")
        )
    }

}
