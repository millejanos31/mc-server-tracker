package com.example.mcservertracker.ui.main

import android.animation.ValueAnimator
import android.view.View

object CustomAnimationUtils {

  fun startAnimateViewHeight(view: View, toHeight: Int, duration: Long) {
    val anim = ValueAnimator.ofInt(view.measuredHeight, toHeight)
    anim.addUpdateListener { valueAnimator ->
      val newValue: Int = valueAnimator.animatedValue as Int
      val params = view.layoutParams
      params.height = newValue
      view.layoutParams = params
    }
    anim.duration = duration;
    anim.start();
  }
}