package com.example.mcservertracker.ui.main.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mcservertracker.ui.main.ServerDataProvider

class ServerViewModelFactory(private val provider: ServerDataProvider)
    : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ServerListViewModel(provider) as T
    }
}