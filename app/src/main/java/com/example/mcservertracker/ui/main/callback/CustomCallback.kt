package com.example.mcservertracker.ui.main.callback

interface CustomCallback <T> {

    fun execute(): T
}