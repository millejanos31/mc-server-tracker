package com.example.mcservertracker.ui.main.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mcservertracker.R
import com.example.mcservertracker.ui.main.*
import com.example.mcservertracker.ui.main.models.ServerUI
import com.example.mcservertracker.ui.main.navigation.Navigation
import com.example.mcservertracker.ui.main.utils.InjectorUtils
import com.example.mcservertracker.ui.main.viewmodel.ServerListViewModel
import io.reactivex.disposables.Disposable
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ServerListFragment : Fragment() {

    private val TAG = ServerListFragment::class.simpleName
    private val REFRESH_COOLDOWN_TIME = 3000;
    private lateinit var mViewModel: ServerListViewModel
    private lateinit var mView: View
    private lateinit var mServerRecyclerView: RecyclerView
    private lateinit var mServerAdapter: ServerRecyclerAdapter
    private lateinit var maddServerTVbutton: TextView
    private lateinit var refreshServersTVbutton: TextView
    private var disposableGetData : Disposable? = null;

    companion object {
        fun newInstance() =
            ServerListFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        mView = inflater.inflate(R.layout.server_list_fragment, container, false)
        initViews()
        val provider = ServerDataProvider.getInstance()
        val factory = InjectorUtils.provideServerViewModelFactory()
        mViewModel = ViewModelProviders.of(this, factory).get(ServerListViewModel(provider)::class.java)
        listenToAddNewServer()
        return mView
    }

    fun initViews() {
        mServerRecyclerView = mView.findViewById(R.id.server_list_recycler_view)
        maddServerTVbutton = mView.findViewById(R.id.add_server_tv_button)
        refreshServersTVbutton = mView.findViewById(R.id.refresh_server_list_tv_button)
        maddServerTVbutton.setOnClickListener(addServerButtonClickListener)
        refreshServersTVbutton.setOnClickListener(refreshServerListClickListener)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecyclerView()
        //addDataSetFake(3, 1)
        val rawServers = mViewModel.getPersistedServerIpList()
        addServerToList(rawServers)
    }

    /**/@Deprecated(message = "Only for debugging! Do not use it in production!")
    /**/private fun addDataSetFake(numberOfData: Int, startCountFrom: Int = 1) {
    /**/    val data =
    /**/        DataSource.createFakeDataSet(
    /**/            numberOfData,
    /**/            startCountFrom
    /**/        )
    /**/    mServerAdapter.add(data)
    /**/}

    /**
     * - Add server to UI list
     * - Load data -> Update UI
     * - Ping server -> Update UI
     * servers pair: IP & Custom Name
     */
    private fun addServerToList(rawServers: List<Pair<String, String>>) {
        addServersWithoutDataToList(rawServers)
        addServersWithDataToList(rawServers)
    }

    private fun addServersWithoutDataToList(rawServers: List<Pair<String, String>>) {
        rawServers.forEach { dnsIp ->
            val server = ServerUI(ip = null, dnsIp = dnsIp.first, isLoading = true, isPinging = true,
                customName = dnsIp.second) // show pinging while loading server
            addToListIfNotExists(server)
        }
    }

    /**
     * Adding server with IP and Custom name to list
     */
    private fun addServerWithDataToList(ip: String, customName: String) {
        addServersWithDataToList(listOf(Pair(ip, customName)))
    }

    /**
     * Adding servers with IP and Custom name to list
     */
    private fun addServersWithDataToList(rawServers: List<Pair<String, String>>) {
        mViewModel.getServersDataByIpsCold(rawServers)
            .subscribeOn(Schedulers.newThread())
            .observeOn(Schedulers.trampoline())
            .subscribe({ serverUI ->
                Log.d(TAG, "Adding data to list: serverUI: $serverUI")
                io.reactivex.rxjava3.core.Observable.just(serverUI)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ serverUI2 ->
                        // TODO: solve double RX here! (change thread, or flatmap)
                        updateServerWithData(serverUI2)
                        pingServer(serverUI2)
                    },
                        { error ->
                            Log.e(TAG, "addDataSet: ERROR: ", Exception(error))
                        })
            },
                { error ->
                    Log.e(TAG, "addDataSet: ERROR: ", Exception(error))
                })
    }

    private fun updateServerWithData(server: ServerUI) {
        Log.d(TAG, "Updating server with data of server: " + server.ip)
        if (server.online == null) {
            server.cardState = ServerUI.Companion.CardState.FAILED
        } else {
            server.cardState = ServerUI.Companion.CardState.DONE
        }
        server.pingState = ServerUI.Companion.PingState.PINGING
        updateCard(server)
    }

    private fun pingServer(server: ServerUI) {
        if (server.dnsIp != null && server.online!!) {
            // has IP && has server info
            mViewModel.getPingServerByIpCold(server.dnsIp!!)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.trampoline())
                .subscribe { ping ->
                    server.ping = ping.toInt();
                    server.pingState = ServerUI.Companion.PingState.SUCCESS
                    updateCard(server)
                }
        } else {
            //server.cardState = ServerUI.Companion.CardState.FAILED
            server.pingState = ServerUI.Companion.PingState.FAILED
            server.ping = ServerUI.Companion.PingLevel.PING_HOST_NOT_FOUND.value
            updateCard(server)
        }
    }

    /**
     * Run this method only on UI thread
     */
    private fun addToListIfNotExists(server: ServerUI) {
        if (mServerAdapter.contains(server.ip, server.dnsIp) || mServerAdapter.contains(server.ip, server.dnsIp)) {
            val index = Utils.getServerIndexByIp(server.dnsIp, mServerAdapter.getAllItems())
            mServerAdapter.update(index, server)
        } else {
            mServerAdapter.add(server)
        }
        mServerRecyclerView.adapter = mServerAdapter // TODO run on UI thread
        //mServerAdapter.notifyDataSetChanged()
        //Log.d(TAG, "update item at: 0 to " + mServerAdapter.itemCount)
        mServerAdapter.notifyItemRangeChanged(0, mServerAdapter.itemCount)
    }

    // TODO update item without re-focusing first card!
    private fun updateCard(update: ServerUI) {
        val index = Utils.getServerIndexByIp(update.dnsIp, mServerAdapter.getAllItems())
        when (index) {
            -1 -> {
                Log.e(TAG, "ERROR: Updatable item not found at index of $index, size of the list: " + mServerAdapter.getAllItems().size)
                return
            } else -> {
                io.reactivex.rxjava3.core.Observable.just(update)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        mServerAdapter.update(index, update)
                        //mServerRecyclerView.adapter = mServerAdapter // TODO run on UI thread
                        ////mServerAdapter.notifyDataSetChanged() // TODO notify only updated item
                        mServerAdapter.notifyItemRangeChanged(index, 1)
                    }
            }
        }
    }

    val addServerButtonClickListener = View.OnClickListener {
        Navigation.showAddServerPopup();
    }

    /**
     * Reload data & ping of the servers
     */
    val refreshServerListClickListener = View.OnClickListener {
        Toast.makeText(mView.context, "TODO: Refresh", Toast.LENGTH_SHORT).show()
        // TODO
    }

    private fun initRecyclerView() {
        mServerRecyclerView.apply{
            layoutManager = LinearLayoutManager(mView.context)
            val topSpacingDecoration =
                TopSpacingItemDecoration(16)
            addItemDecoration(topSpacingDecoration)
            mServerAdapter =
                ServerRecyclerAdapter()
            adapter = mServerAdapter
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposableGetData?.dispose()
    }

    private fun scrollToServer(ip: String) {
        // TODO scroll to new server
        //val index = mServerAdapter.getServerIndexByIp(ip)
        //mServerRecyclerView.apply {
        //    layoutManager!!.scrollToPosition(index)
        //}
    }

    private fun listenToAddNewServer() {
        SubjectManager.addNewServerSubject
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.trampoline())
            .subscribe {
                val serverName = it.first
                val serverIp = it.second
                addServerToList(listOf(Pair(serverIp, serverName)))
                Utils.hideKeyboard(this.activity!!)
                Toast.makeText(context, "Added server $serverIp", Toast.LENGTH_LONG).show()
                scrollToServer(serverIp)
            }
    }

}
