package com.example.mcservertracker.ui.main.utils

import com.example.mcservertracker.ui.main.ServerDataProvider
import com.example.mcservertracker.ui.main.viewmodel.ServerViewModelFactory

object InjectorUtils {

    fun provideServerViewModelFactory(): ServerViewModelFactory {
        val provider = ServerDataProvider.getInstance(/*DB.getInstance().dataAccessObject*/)
        return ServerViewModelFactory(provider)
    }



}