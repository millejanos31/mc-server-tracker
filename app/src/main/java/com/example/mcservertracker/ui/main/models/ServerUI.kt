package com.example.mcservertracker.ui.main.models

import com.example.mcservertracker.R
import com.example.mcservertracker.ui.main.Utils
import com.example.mcservertracker.ui.main.models.ServerUI.Companion.PingLevel.*

/*data*/ class ServerUI {

    companion object {
        enum class CardState {
            NONE, LOADING, FAILED, DONE
        }
        enum class PingState {
            NONE, PINGING, FAILED, SUCCESS
        }
        enum class PingLevel(val value:Int) {
            PING_HOST_NOT_FOUND(-1),
            PING_LEVEL_5(50),
            PING_LEVEL_4(100),
            PING_LEVEL_3(200),
            PING_LEVEL_2(300),
            PING_LEVEL_1(Integer.MAX_VALUE)
        }
        const val PLAYER_COUNT_SEPARATOR = "/"
        const val PLAYER_COUNT_UNKNOWN = "-"
    }
    var dnsIp: String? = null
    var ip: String? = null
    var customName: String? = null
    var motd: Motd? = null
    var image: String? = null
    var players: Players? = null
    var ping: Int? = null
    var online: Boolean? = null
    var cardState: CardState = CardState.NONE
    var pingState: PingState = PingState.NONE
    //val enteredId: String // To re-display the correct user-entered ID in EDIT Server mode
    //val enteredIdType: EnteredIdType = EnteredIdType.UNKNOWN
    //enum class EnteredIdType() {
    //    UNKNOWN, IP, DNS_IP
    //}
    var open: Boolean = false
        set(value) {
            if (field != value) {
                field = value
            }
    }

    constructor(bo: ServerResponse, dnsIp: String, customName: String) {
        this.dnsIp = dnsIp
        this.ip = bo.getIp()
        this.customName = customName;
        this.motd = bo.getMotd()
        this.image = bo.getIcon()
        this.players = bo.getPlayers()
        this.ping = PING_HOST_NOT_FOUND.value
        this.online = bo.isOnline()
    }

    constructor(name: String?, ip: String?, motd: Motd?, image: String?, players: Players?, ping: Int?, online: Boolean) {
        this.dnsIp = name
        this.ip = ip
        this.customName = null
        this.motd = motd
        this.image = image
        this.players = players
        this.ping = ping
        this.online = online
    }

    /**
     * Initial card with basic data (Ip, Card & Ping state)
     */
    constructor(ip: String?, dnsIp: String?, isLoading: Boolean, isPinging: Boolean, customName: String) {
        this.ip = ip;
        this.dnsIp = dnsIp;
        this.customName = customName
        if (isLoading) {
            this.cardState = CardState.LOADING
        } else {
            this.cardState = CardState.NONE
        }
        if (isPinging) {
            this.pingState = PingState.PINGING
        } else {
            this.pingState = PingState.NONE
        }
    }

    fun getFormattedPlayerString(): String {
        if (players == null) {
            return ""
        }

        val online: Int? = players!!.onlinePlayerCount
        val limit: Int? = players!!.maxPlayerCount

        if (isValidPlayerOnlineCount(online) && isValidPlayerLimitCount(limit)) {
            return Utils.getInThousands(online) + PLAYER_COUNT_SEPARATOR + Utils.getInThousands(limit)
        }

        if (isValidPlayerOnlineCount(online)) {
            return Utils.getInThousands(online) + PLAYER_COUNT_SEPARATOR + PLAYER_COUNT_UNKNOWN
        }

        if (isValidPlayerLimitCount(limit)) {
            return PLAYER_COUNT_UNKNOWN + PLAYER_COUNT_SEPARATOR + Utils.getInThousands(limit)
        }

        return PLAYER_COUNT_UNKNOWN + PLAYER_COUNT_SEPARATOR + PLAYER_COUNT_UNKNOWN
    }

    private fun isValidPlayerOnlineCount(num: Int?): Boolean {
        return !(num == null || num < 0)
    }

    private fun isValidPlayerLimitCount(num: Int?): Boolean {
        return !(num == null || num < 1)
    }

    fun getFormattedPing():String = when(pingState) {
        PingState.NONE -> "-"
        PingState.PINGING -> ""
        PingState.FAILED -> ""
        PingState.SUCCESS -> ping?.toString() + "ms"
    }

    fun getFormattedMotd(): String {
        var htmlRows = ""
        if (motd?.html != null) {
            if (motd!!.html!!.isNotEmpty()) {
                htmlRows += motd!!.html!![0]
            }
            if (motd!!.html!!.size > 1) {
                htmlRows += "<br>" + motd!!.html!![1]
            }
        }
        return htmlRows
    }

    fun getPingLevelDrawable(): Int {
        if (ping != null && ping!! > 0) {
            if (ping!! <= PING_LEVEL_5.value) {
                return R.drawable.ping_signal_5_hd
            }
            if (ping!! <= PING_LEVEL_4.value) {
                return R.drawable.ping_signal_4_hd
            }
            if (ping!! <= PING_LEVEL_3.value) {
                return R.drawable.ping_signal_3_hd
            }
            if (ping!! <= PING_LEVEL_2.value) {
                return R.drawable.ping_signal_2_hd
            }
            if (ping!! <= PING_LEVEL_1.value) {
                return R.drawable.ping_signal_1_hd
            }
        }
        return R.drawable.ping_signal_0_hd
    }

    fun hasSameIpAs(serverIp: String): Boolean {
        return serverIp == this.ip
            || serverIp == this.dnsIp
    }

    override fun toString(): String {
        return "${this.javaClass.simpleName} + {" +
            "dnsIp: $dnsIp, " +
            "ip: $ip, " +
            "customName: $customName, " +
            "motd: $motd, " +
            "image: $image, " +
            "players: $players, " +
            "ping: $ping, " +
            "cardState: $cardState, " +
            "cardState: $cardState, " +
            "pingState: $pingState, " +
            "open: $open" +
            "}"
    }
}
