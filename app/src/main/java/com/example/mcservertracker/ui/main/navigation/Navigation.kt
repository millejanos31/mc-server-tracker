package com.example.mcservertracker.ui.main.navigation

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.mcservertracker.ui.main.view.AddServerPopupFragment
import com.example.mcservertracker.ui.main.view.MainActivity
import com.example.mcservertracker.ui.main.view.ServerListFragment
import com.example.mcservertracker.ui.main.view.SettingsFragment


object Navigation {

    lateinit var mContext: Context


    fun init (context: Context){
        mContext = context
    }


    fun showMainScreen() {
        rrrrrrrreplaceFragment(
            ServerListFragment.newInstance(),
            com.example.mcservertracker.R.id.main_container
        )
    }

    fun showAddServerPopup() {
        rrrrrrrreplaceFragment(
            AddServerPopupFragment.newInstance(),
            com.example.mcservertracker.R.id.popup_container
        )
    }

    fun showSettingsScreen() {
        rrrrrrrreplaceFragment(
            SettingsFragment.newInstance(),
            com.example.mcservertracker.R.id.main_container // TODO: use proper container
        )
    }

    private fun showInitialFragment(fragment: Fragment, container: Int) {
        val act: MainActivity = (mContext as MainActivity)

        act.setContentView(com.example.mcservertracker.R.layout.main_activity)

        act.supportFragmentManager.beginTransaction()
            .add(container, fragment)
            .commitNow()
    }

    private fun replaceFragment(fragment: Fragment, container: Int) {

        val act: MainActivity = (mContext as MainActivity)

        //act.setContentView(R.layout.main_activity)
        //if (act.savedInstanceState == null) {
        act.supportFragmentManager.beginTransaction()
            .replace(container, fragment)
            //.addToBackStack(fragment::class.simpleName)
            .commitNow()
        //}
    }

    fun hidePopup() {
        val act: MainActivity = (mContext as MainActivity)
        act.supportFragmentManager.popBackStack()
    }

    private fun rrrrrrrreplaceFragment(fragment: Fragment, container: Int) {
        val backStateName = fragment.javaClass.name
        val manager = (mContext as MainActivity).supportFragmentManager
        val fragmentPopped: Boolean = manager.popBackStackImmediate(backStateName, 0)
        if (!fragmentPopped) { //fragment not in back stack, create it.
            val ft: FragmentTransaction = manager.beginTransaction()
            ft.replace(container, fragment)
            ft.addToBackStack(backStateName)
            ft.commit()
        }
    }

}