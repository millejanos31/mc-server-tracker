package com.example.mcservertracker.ui.main

import com.example.mcservertracker.ui.main.models.Motd
import com.example.mcservertracker.ui.main.models.Players
import com.example.mcservertracker.ui.main.models.ServerUI

class DataSource {

    companion object {

        @Deprecated(message = "Only for debugging! Do not use it in production!")
        fun createFakeDataSet(numberOfData: Int, startCountFrom: Int = 1): ArrayList<ServerUI>{
            val serverList: ArrayList<ServerUI> = ArrayList()
            val htmlMotdExample: String = "\u00a71Example \u00a72MOTD \u00a78|\u00a7a|\u00a7r|\u00a70|\u00a7r|||\u00a7d|\u00a7r|\u00a76|'\u00a7e\u00a7k\\/\u00a7r\n" +
                    "\u00a78\u00a7oFor\u00a79\u00a7l Minecraft\u00a7r \u00a7d\u00a7n-\u00a74 Join \u00a74\u00a7mnow\u00a76!"
            for (i in startCountFrom until startCountFrom + numberOfData) {
                serverList.add(
                    ServerUI(
                        "ServerName$i",
                        "Id$i",
                        Motd(
                            listOf("Example MOTD\nFor Minecraft"),
                            listOf("Example MOTD\nFor Minecraft"),
                            listOf(htmlMotdExample)
                            //listOf("motd1raw$i", "motd2raw$i"),
                            //listOf("motd1celan$i"),
                            //listOf("motd1html$i")
                        ),
                        "imagePath$i",
                        Players(i, i * 10, listOf("aA$i")),
                        i * 10,
                        (i % 3 == 0)
                    )
                )
            }
            return serverList
        }

    }
}