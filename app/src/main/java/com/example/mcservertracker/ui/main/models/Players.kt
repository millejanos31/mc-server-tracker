package com.example.mcservertracker.ui.main.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class Players {

    @SerializedName("online")
    @Expose
    var onlinePlayerCount: Int?
        get() = field
        set(value) { field = value }

    @SerializedName("max")
    @Expose
    var maxPlayerCount: Int?

    @SerializedName("list") // Only included when there are any players
    @Expose
    var onlinePlayers: List<String?>?


    constructor() {
        this.onlinePlayerCount = -1
        this.maxPlayerCount = -1
        this.onlinePlayers = null
    }

    constructor(onlinePlayerCount: Int, maxPlayerCount: Int, onlinePlayers: List<String>?) {
        this.onlinePlayerCount = onlinePlayerCount
        this.maxPlayerCount = maxPlayerCount
        this.onlinePlayers = onlinePlayers
    }

    override fun toString(): String {
        return "Players{" +
                "onlinePlayerCount=" + onlinePlayerCount +
                ", maxPlayerCount=" + maxPlayerCount +
                ", onlinePlayers=" + onlinePlayers +
                '}'
    }
}