package com.example.mcservertracker.ui.main.view

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.mcservertracker.R
import com.example.mcservertracker.ui.main.navigation.Navigation
import kotlin.system.exitProcess


class MainActivity : AppCompatActivity() {

    private val TAG = MainActivity::class.simpleName
    private lateinit var mContext: MainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       setContentView(R.layout.main_activity);

        mContext = this;
        Navigation.init(mContext)
        Navigation.showMainScreen()
    }

}
