package com.example.mcservertracker.ui.main.retrofitstuff

import com.example.mcservertracker.ui.main.models.PingResponse
import com.example.mcservertracker.ui.main.models.ServerResponse
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path

interface MCServerApi {

    @GET("2/{ip}")
    fun getServer(
        @Path("ip") serverIp: String?
    ): Flowable<ServerResponse>?

    @GET("ping/{ip}")
    fun pingServer(
        @Path("ip") serverIp: String?
    ): Flowable<PingResponse>?
}