package com.example.mcservertracker.ui.main.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class Motd {

    @SerializedName("raw")
    @Expose
    var raw: List<String?>?

    @SerializedName("clean")
    @Expose
    var clean: List<String?>?

    @SerializedName("html")
    @Expose
    var html: List<String?>?


    constructor() {
        this.raw = null
        this.clean = null
        this.html = null
    }

    constructor(raw: List<String?>?, clean: List<String?>?, html: List<String?>?) {
        this.raw = raw
        this.clean = clean
        this.html = html
    }

    override fun toString(): String {
        return "Motd{" +
                "raw=" + raw +
                ", clean=" + clean +
                ", html=" + html +
                '}'
    }
}