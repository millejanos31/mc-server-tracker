package com.example.mcservertracker.ui.main

import android.util.Log
import com.example.mcservertracker.ui.main.models.ServerUI
import com.example.mcservertracker.ui.main.retrofitstuff.MCServerApi
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

// TODO use base class to allow switching between providers
object ServerDataProvider /*: BaseServerDataProvider*/ {

    private const val BASE_API: String = "https:/api.mcsrvstat.us/"
    private val TAG = ServerDataProvider::class.simpleName
    private var mInstance: ServerDataProvider
    private var mApi: MCServerApi? = null

    init {
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(BASE_API)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        mApi = retrofit.create(MCServerApi::class.java)
        mInstance = this
    }

    fun getInstance(): ServerDataProvider {
        return mInstance
    }

    fun getServerDataByDnsIp(dnsIp: String, customName: String): Observable<ServerUI> {
        Log.d(TAG, "getServerDataByIp: Getting server info of IP: \"$dnsIp\"")
        return mApi!!.getServer(dnsIp)
            ?.map {
                ServerUI(bo = it, dnsIp = dnsIp, customName = customName)
            }
            ?.filter { t -> !t.ip.isNullOrEmpty() }
            ?.toObservable()!!
    }

    fun pingServerByDnsIp(dnsIp: String): Observable<Long> {
        val start = System.nanoTime()
        val response = mApi!!.pingServer(dnsIp)
        val end = System.nanoTime()

        val ping: Long? = when (response) {
            null -> ServerUI.Companion.PingLevel.PING_HOST_NOT_FOUND.value.toLong()
            else -> (end - start)/1000 // TODO: Find better way to ping server (precise ping)
        }
        return Observable.just(ping)
    }

}