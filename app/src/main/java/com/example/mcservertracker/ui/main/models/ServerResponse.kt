package com.example.mcservertracker.ui.main.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class ServerResponse {

    companion object {
        const val DEFAULT_IP = "127.0.0.1"
    }

    @SerializedName("online")
    @Expose
    private var isOnline:Boolean = false

    /**
     * DNS IP Address
     */
    @SerializedName("ip")
    @Expose
    private var ip: String? = null

    /**
     * Note: If API call success: Int, if not: String
     */
    @SerializedName("port")
    @Expose
    private var port:String? = ""

//    @SerializedName("debug")
//    @Expose
//    private Debug debug;

    //    @SerializedName("debug")
//    @Expose
//    private Debug debug;
    @SerializedName("motd")
    @Expose
    private var motd: Motd? = null

    @SerializedName("players")
    @Expose
    private var players: Players? = null

    @SerializedName("version") // Could include multiple versions or additional text
    @Expose
    private var version: String? = null

    @SerializedName("protocol") // Only included when ping is used, see more here: http://wiki.vg/Protocol_version_numbers
    @Expose
    private var protocol: Int? = 0

    @SerializedName("hostname") // Only included when a hostname is detected
    @Expose
    private var hostname: String? = null

    @SerializedName("icon") // Only included when an icon is detected
    @Expose
    private var icon: String? = null

    @SerializedName("software") // Only included when software is detected
    @Expose
    private var software: String? = null

    @SerializedName("map") // Only included when the value is not "world"
    @Expose
    private var map: String? = null

//    @SerializedName("plugins")          // Only included when plugins are detected
//    @Expose
//    private Plugins plugins;

//    @SerializedName("mods")             // Only included when mods are detected
//    @Expose
//    private Mods mods;

//    @SerializedName("info")             // Only included when detecting that the player samples are used for information
//    @Expose
//    private Info info;


    //    @SerializedName("plugins")          // Only included when plugins are detected
//    @Expose
//    private Plugins plugins;
//    @SerializedName("mods")             // Only included when mods are detected
//    @Expose
//    private Mods mods;
//    @SerializedName("info")             // Only included when detecting that the player samples are used for information
//    @Expose
//    private Info info;
    fun isOnline(): Boolean {
        return isOnline
    }

    fun setOnline(online: Boolean) {
        isOnline = online
    }

    fun getIp(): String? {
        return ip
    }

    fun setIp(ip: String?) {
        this.ip = ip
    }

    fun getPort(): String? {
        return port
    }

    fun setPort(port: String?) {
        this.port = port
    }

    fun getMotd(): Motd? {
        return motd
    }

    fun setMotd(motd: Motd?) {
        this.motd = motd
    }

    fun getPlayers(): Players? {
        return players
    }

    fun setPlayers(players: Players?) {
        this.players = players
    }

    fun getVersion(): String? {
        return version
    }

    fun setVersion(version: String?) {
        this.version = version
    }

    fun getProtocol(): Int? {
        return protocol
    }

    fun setProtocol(protocol: Int) {
        this.protocol = protocol
    }

    fun getHostname(): String? {
        return hostname
    }

    fun setHostname(hostname: String?) {
        this.hostname = hostname
    }

    fun getIcon(): String? {
        return icon
    }

    fun setIcon(icon: String?) {
        this.icon = icon
    }

    fun getSoftware(): String? {
        return software
    }

    fun setSoftware(software: String?) {
        this.software = software
    }

    fun getMap(): String? {
        return map
    }

    fun setMap(map: String?) {
        this.map = map
    }

    override fun toString(): String {
        return "Server{" +
                "isOnline=" + isOnline +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                ", motd=" + motd +
                ", players=" + players +
                ", version='" + version + '\'' +
                ", protocol=" + protocol +
                ", hostname='" + hostname + '\'' +
                ", icon='" + icon + '\'' +
                ", software='" + software + '\'' +
                ", map='" + map + '\'' +
                '}'
    } 
}