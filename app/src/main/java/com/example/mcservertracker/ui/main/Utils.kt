package com.example.mcservertracker.ui.main

import android.Manifest
import android.R
import android.annotation.TargetApi
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.mcservertracker.ui.main.models.ServerUI


class Utils {

    companion object {

        // TODO: App Permissions

        private const val MY_PERMISSIONS_REQUEST_INTERNET = 123
        private val TAG = Utils::class.simpleName

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        fun checkPermission(context: Context): Boolean {
            val currentAPIVersion = Build.VERSION.SDK_INT
            return if (currentAPIVersion >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(
                        context,
                        Manifest.permission.INTERNET
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            (context as Activity?)!!,
                            Manifest.permission.INTERNET
                        )
                    ) {
                        val alertBuilder: AlertDialog.Builder = AlertDialog.Builder(context)
                        alertBuilder.setCancelable(true)
                        alertBuilder.setTitle("Permission necessary")
                        alertBuilder.setMessage("External storage permission is necessary")
                        alertBuilder.setPositiveButton(
                            R.string.yes,
                            DialogInterface.OnClickListener { dialog, which ->
                                ActivityCompat.requestPermissions(
                                    (context as Activity?)!!,
                                    arrayOf(Manifest.permission.INTERNET),
                                    MY_PERMISSIONS_REQUEST_INTERNET
                                )
                            })
                        val alert: AlertDialog = alertBuilder.create()
                        alert.show()
                    } else {
                        ActivityCompat.requestPermissions(
                            (context as Activity?)!!,
                            arrayOf(Manifest.permission.INTERNET),
                            MY_PERMISSIONS_REQUEST_INTERNET
                        )
                    }
                    false
                } else {
                    true
                }
            } else {
                true
            }
        }

        fun getInThousands(num: Int?): String{
            return if (num != null) {
                if (num > 999) {
                    num.div(1000).toString() + "k"
                } else {
                    num.toString()
                }
            } else {
                "-"
            }
        }


        // TODO: Maybe integrate into adapter?
        /**
         * @return the index of the server with the given IP or DNS IP.
         * Return -1 otherwise.
         */
        fun getServerIndexByIp(ip: String?, serverCards: ArrayList<ServerUI>) : Int {
            if (ip != null) {
                serverCards.forEachIndexed { index, server ->
                    if (server.hasSameIpAs(ip)) {
                        return index
                    }
                }
            }
            Log.e(TAG, "ERROR: getServerIndexByDNSIp: NOT FOUND: $ip")
            return -1;
        }

        fun hideKeyboard(activity: Activity) {
            val imm: InputMethodManager =
                activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            //Find the currently focused view, so we can grab the correct window token from it.
            var view: View? = activity.currentFocus
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

    }

}