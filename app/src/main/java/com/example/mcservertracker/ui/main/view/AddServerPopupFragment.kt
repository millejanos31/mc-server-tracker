package com.example.mcservertracker.ui.main.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.example.mcservertracker.R
import com.example.mcservertracker.ui.main.SubjectManager
import com.example.mcservertracker.ui.main.navigation.Navigation

// After closing the popup with Add or Close buttons
// the entered data will be passed to the ViewModel with the closing status
class AddServerPopupFragment : Fragment() {

    val TAG = AddServerPopupFragment::class.simpleName
    private lateinit var mView: View
    private lateinit var mServerNameEditText: EditText
    private lateinit var mServerIPEditText: EditText
    private lateinit var mServerAddTVButton: TextView
    private lateinit var mServerCancelTVButton: TextView
    private lateinit var mGrayedCloseBackground: ConstraintLayout

    companion object {
        fun newInstance() =
            AddServerPopupFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        mView = inflater.inflate(R.layout.add_server_popup_fragment, container, false)
        initViews()
        return mView
    }

    private fun initViews() {
        mServerNameEditText = mView.findViewById(R.id.add_server_popup_name_field_edit_text)
        mServerIPEditText = mView.findViewById(R.id.add_server_popup_ip_field_edit_text)
        mServerAddTVButton = mView.findViewById(R.id.add_server_popup_add_tv_button)
        mServerCancelTVButton = mView.findViewById(R.id.add_server_popup_close_tv_button)
        mServerAddTVButton.setOnClickListener(addButtonListener)
        mServerCancelTVButton.setOnClickListener(closeButtonListener)
        // should be in lower level:
        //mGrayedCloseBackground = mView.findViewById(R.id.grayed_close_background)
        //mGrayedCloseBackground.setOnClickListener(closeButtonListener)
    }

    private val addButtonListener = View.OnClickListener {
        val newServer = Pair(mServerNameEditText.text.toString(), mServerIPEditText.text.toString())
        SubjectManager.addNewServerSubject.onNext(newServer)
        closePopup()
    }


    private val closeButtonListener = View.OnClickListener {
        closePopup()
    }

    private fun closePopup() {
        Navigation.hidePopup()
    }

}